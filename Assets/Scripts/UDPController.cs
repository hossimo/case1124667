﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Runtime.InteropServices;
using System;
using System.Net;
using System.Net.Sockets;

public class UDPController : MonoBehaviour
{
    const int PORT = 6454;
    private Thread udpReceiverThread;
    private bool stop = false;
    private Dictionary<int, ArtNetPacket> currentPackets;

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct ArtNetPacket
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string ID;
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public UInt16 OpCode;                               // See Doc. Table 1 - OpCodes eg. 0x5000 OpOutput / OpDmx
        [MarshalAs(UnmanagedType.U2, SizeConst = 2)]
        public UInt16 version;                              // 0x0e00 (aka 14)
        [MarshalAs(UnmanagedType.U1)]
        public byte seq;                                    // monotonic counter
        [MarshalAs(UnmanagedType.U1)]
        public byte physical;                               // 0x00
        [MarshalAs(UnmanagedType.U1)]
        public byte subUni;                                 // low universe (0-255)
        [MarshalAs(UnmanagedType.U1)]
        public byte net;                                    // high universe (not used)
        [MarshalAs(UnmanagedType.U2)]
        public UInt16 length;                               // data length (2 - 512)
        //[MarshalAs(UnmanagedType.ByValArray, SizeConst = 513, ArraySubType = UnmanagedType.U1)]   // <-- This Works
          [MarshalAs(UnmanagedType.U1, SizeConst = 513)]                                            // <-- This causes crash when packet is received.
        public byte[] data;                                 // universe data
    }

    /*
     *  START 
     */
    void Start()
    {
        currentPackets = new Dictionary<int, ArtNetPacket>();
        udpReceiverThread = new Thread(UdpReceiverThread) { IsBackground = true };
        udpReceiverThread.Start();
    }

    /*
     *  UdpReceiverThread
     */
    public void UdpReceiverThread()
    {
        Debug.Log("Starting UDP Receiver Thread...");
        IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
        UdpClient udpClient = new UdpClient(PORT);
        //udpClient.Client.ReceiveTimeout = 2000;  <- Two Second timeout not needed for test

        while (!stop)
        {
            try
            {
                // grab a packet
                Byte[] bytes;
                bytes = udpClient.Receive(ref remoteIP);
                Debug.Log("Got Packet - " + bytes.Length + "bytes");

                // store it in the database
                ArtNetPacket packet = ByteArrayToArtNetPacket(bytes);
                currentPackets[(int)packet.subUni] = packet;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    /*
     *  Bit-Converter
     */

    private ArtNetPacket ByteArrayToArtNetPacket(byte[] byteArray)
    {
        GCHandle handle = GCHandle.Alloc(byteArray, GCHandleType.Pinned);
        ArtNetPacket result;
        try
        {
            result = (ArtNetPacket)Marshal.PtrToStructure(handle.AddrOfPinnedObject(), typeof(ArtNetPacket));
        }
        catch (Exception)
        {
            throw;
        }
        finally
        {
            handle.Free();
        }
        return result;
    }


    void Update()
    {
        
    }

    private void OnDestroy()
    {
        stop = true;
    }
}
